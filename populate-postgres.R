#!/usr/bin/env Rscript

#  testdata_to_postgres.r
#
#
#  Created by asdf on 24.05.21.
#

library(curl)
library(httr)
library(jsonlite)

MHmakeRandomString <- function(n = 1, lenght = 12)
{
  randomString <- c(1:n)
  for (i in 1:n)
  {
    randomString[i] <- paste(sample(c(0:9, letters, LETTERS),
                                    lenght, replace = TRUE),
                             collapse = "")
  }
  return(randomString)
}


DFgen <- DFmaker <- function(n = 10,
                             type = wide,
                             digits = 2,
                             proportion = FALSE,
                             na.rate = 0) {
  rownamer <- function(dataframe) {
    x <- as.data.frame(dataframe)
    rownames(x) <- NULL
    return(x)
  }
  
  dfround <- function(dataframe, digits = 0) {
    df <- dataframe
    df[, sapply(df, is.numeric)] <-
      round(df[, sapply(df, is.numeric)], digits)
    return(df)
  }
  
  TYPE <- as.character(substitute(type))
  figure = sample(1:100, n, replace = TRUE) + abs(rnorm(n))
  DF <- data.frame(
    #id = paste0("ID.", 1:n),
    id = paste0(1:n),
    #dates = format(seq(ISOdate(2003,1,1), by='month', length=n), format='%d.%m.%Y'),
    evaldate = format(seq(
      ISOdate(1920, 1, 1), by = 'hour', length = n
    ), format = '%Y-%m-%d'),
    group = sample(c("possible threat", "control"), n, replace = TRUE),
    gender = sample(
      c(
        "Gender fluid",
        "Non-binary",
        "Genderqueer",
        "Transgender",
        "Cisgender",
        "Male",
        "Female"
      ),
      n,
      replace = TRUE
    ),
    age = sample(18:40, n, replace = TRUE),
    status = sample(
      c(
        "Single",
        "Roommates",
        "Cohabitants",
        "Married",
        "Separated",
        "Divorced"
      ),
      n,
      replace = TRUE,
      prob = c(.20, .4, .3, .05, .05, .24)
    ),
    political = sample(
      c(
        "Centerpoint Party",
        "Human League",
        "Loyalists",
        "Rationalist Party",
        "Rights of Sentience Party",
        "Separatists"
      ),
      n,
      replace = TRUE
    ),
    species = sample(
      c(
        "Togruta",
        "Psadan",
        "Shi'ido",
        "Trandoshan",
        "Squib",
        "Ruurian",
        "P'w'eck",
        "Rodian",
        "Sanyassan",
        "Twi'lek",
        "Selkath",
        "Toydarian",
        "Rakata",
        "Pa'lowick"
      ),
      n,
      replace = TRUE
    ),
    number_of_children = rpois(n, lambda = 1.2),
    spending_behavior = sample(c(
      seq(0, 200, by = 12),
      seq(0, 268400, by = 120)
    ), n, replace = TRUE),
    
    score = sample(c(seq(
      -268, 268, by = dnorm((12) * 4 / 126)
    )), n, replace = TRUE),
    figure,
    asdf = MHmakeRandomString(n)
  )
  if (proportion) {
    DF <- cbind (DF[, 1:10],
                 props(
                   ncol = 3,
                   nrow = n,
                   var.names = c(species)
                 ),
                 DF[, 11:14])
  }
  if (na.rate != 0) {
    DF <- cbind(DF[, 1, drop = FALSE], NAins(DF[,-1],
                                             prop = na.rate))
  }
  DF <- switch(TYPE,
               wide = DF,
               long = {
                 DF <- reshape(
                   DF,
                   direction = "long",
                   idvar = "id",
                   v.names = c("value"),
                   timevar = "figure"
                 )
                 rownamer(DF)
               },
               stop("Invalid Data \"type\""))
  return(dfround(DF, digits = digits))
}

#DFgen()
#DFgen(type = "long")
#DFmaker(20000)
#DFgen(prop = T)
#DFgen(na.rate = .3)

#'@author bind data to response
response <- DFmaker(12626)
#vjar = jsonlite::toJSON(response)
#jsonlite::write_json(x = response, path = "Desktop/logger/pubs.json", pretty = TRUE)

#write.table(x = response, sep = "\t", file = "Desktop/develop/javscrip/csvels/asdf.csv", quote = FALSE, row.names = FALSE)



library(jsonlite)
#response <- jsonlite::read_json("/Users/asdf/Desktop/reso.json", simplifyVector = TRUE)


#'@describeIn Wrapper Classes, Cleansing & Datastaging
dataWrapper <- setRefClass(
  "dataWrapper",
  fields = list(data = "data.frame",
                edits = "list"),
  methods = list(
    edit = function(i, j, value) {
      # the following string documents the edit method
      'Replaces the range [i, j] of the object by value.'
      backup <- list(i, j, data[i, j])
      data[i, j] <<- value
      edits <<- c(edits, list(backup))
      invisible(value)
    },
    undo = function() {
      'Undoes the last edit() operation
      and update the edits field accordingly.'
      prev <- edits
      if (length(prev))
        prev <- prev[[length(prev)]]
      else
        stop("No more edits to undo")
      edit(prev[[1]], prev[[2]], prev[[3]])
      ## trim the edits list
      length(edits) <<- length(edits) - 2
      invisible(prev)
    },
    show = function() {
      'Method for automatically printing matrix editors'
      cat("Reference matrix editor object of class",
          classLabel(class(.self)),
          "\n")
      cat("Data: \n")
      methods::show(data)
      cat("Undo list is of length", length(edits), "\n")
    }
  )
)

#'@description transfer Matrix to S4 Object'
#framed <- dataWrapper(data = globalData$records)
#framed2 <- dataWrapper(data = globalData)
'take some slice of the 2 dim matrix'
# 'result <- framed$data[1:991, 1:14]'
# result <- framed$data
#framed2 <- dataWrapper(data = response)
#result <- framed2$data

#'@concept generated data to result
result <- response

#'@author datatransformation & mapping'
'dplyr structural analysis'
#'@example use @dplyr for Structure Analysis
library(dplyr)
result %>% summary.default %>% as.data.frame %>%
  dplyr::group_by(Var1) %>%  tidyr::spread(key = Var2, value = Freq)


#'@capture the output of the Structure'
rcap <- capture.output(str(result))
structured <- dataWrapper(data = data.frame(rcap))




#'@concept select data for index & or db tables'
library(dplyr)
#'@concept select all instead of the nested object'
#selection <-
#result %>% select(!geburtsdatum2) ## deselect the nested frame
'unfortunately elastic can`t handle ""- String in Date Columns.
Therefore the ""- String has to be replaces with "<NA>".'

selection <- result %>% select_all()

require(dplyr)
## fake blank cells
# selection[1, 1] = ""

## define a helper function
#'@usage null reduce fields
empty_as_na <- function(x) {
  if ("factor" %in% class(x))
    x <- as.character(x) ## since ifelse wont work with factors
  ifelse(as.character(x) != "", x, NA)
}

## transform all columns into a frame object
cleaned = selection %>% mutate_each(funs(empty_as_na))

# write.table(
#   x = cleaned,
#   sep = ",",
#   file = "/Users/asdf/Desktop/testfile.csv",
#   quote = TRUE,
#   row.names = FALSE
# )

exists("response") && is.data.frame(get("response"))
#base::remove(response, result, selection)

#'@backref list all dataframes in env.
env <<- eapply(.GlobalEnv, is.data.frame)
envmeta <- names(x = env)

dropper <- function(vector) {
  for (i in 1:length(vector)) {
    vector[i] <- vector[i] + i
  }
  vector
}

i <- 1
j <- as.integer(length(env))
while (i <= j) {
  #print(i)
  print(env)
  i = i + 1
}


print(cleaned)
# write.csv(cleaned, "Desktop/testfile.csv", row.names = FALSE, quote = TRUE, sep = ",")



library(DBI)
#https://github.com/r-dbi/RPostgres
# Connect to a specific postgres database i.e. Heroku
dbObj <- dbConnect(
  RPostgres::Postgres(),
  dbname = 'asdf',
  host = 'localhost',
  # i.e. 'ec2-54-83-201-96.compute-1.amazonaws.com'
  port = 5432,
  # or any other port specified by your DBA
  user = 'postgres',
  password = 'postgres'
)

# list all current tables in the database
RPostgreSQL::dbListTables(conn = dbObj)

# automatically creates the tables-structure & insert data
RPostgreSQL::dbWriteTable(conn = dbObj, name = "microservice", cleaned)

#RPostgreSQL::postgresqlWriteTable(con = dbObj, name = "testing2", value = cleaned)
#
# #https://www.r-bloggers.com/2015/05/getting-started-with-postgresql-in-r/
