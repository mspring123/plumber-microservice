#!/bin/sh

#  Script.sh
#  
#
#  Created by asdf on 24.05.21.
#  
echo "Enter directory name"
read dirname

if [ ! -d "$dirname" ]
then
    echo "File doesn't exist. Creating now"
    mkdir ./$dirname
    echo "File created"
    cd $dirname
        mkdir "plumber-service"
        cd "plumber-service"
            touch "dockerfile"
            touch make_model.R
            touch rest_controller.R
            touch Main.R
            touch get_postgres.R
else
    echo "File exists"
fi

